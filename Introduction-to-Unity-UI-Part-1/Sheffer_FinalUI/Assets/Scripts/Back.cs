﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Back : MonoBehaviour
{
    public GameObject Menu;
    public GameObject Settings;

    public void GoBack()
    {
        if (Menu != null)
        {
            bool isActive = Menu.activeSelf;
            Settings.SetActive(isActive);
            Menu.SetActive(!isActive);
        }

    }

}
