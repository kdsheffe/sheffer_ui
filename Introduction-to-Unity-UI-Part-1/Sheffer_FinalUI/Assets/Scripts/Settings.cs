﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    public GameObject Panel;
    public GameObject MenuPanel;

    public void OpenPanel()
    {
        if (Panel != null)
        {
            bool isActive = Panel.activeSelf;
            MenuPanel.SetActive(isActive);
            Panel.SetActive(!isActive);
        }
  
    }
    
}
